module Models

type State = State of string
type Symbol = Symbol of char
type Transition = Transition of (State * Symbol) * State
type DFA = Symbol list * State list * State * Transition list * State list

module Transition =
    let symbol (Transition ((_, s), _)) = s
    let states (Transition ((q0, _), q1)) = [q0; q1]
    let fst (Transition (f, _)) = f
    let strip (Transition (f, s)) = (f, s)

module DFA =
    let (|DFA|) (s: Symbol list, q: State list, q0: State, ts: Transition list, a: State list) = DFA (s, q, q0, ts, a)
    
    let private cartesian xs ys =
        seq {
            for x in xs do
                for y in ys do
                    yield (x, y);
        } |> Seq.toList
    let private validate (DFA (sigma, q, q0, ts, a)) =
        not (List.isEmpty q)
        && List.contains q0 q
        && Set.isSubset (Set.ofList a) (Set.ofList q)
        && (cartesian q sigma) = (ts |> List.map Transition.fst) 

    let from ((q0, a), ts) =
        let sigma = ts |> List.map Transition.symbol |> List.distinct
        let q = ts |> List.map Transition.states |> List.reduce List.append |> List.distinct
        DFA (sigma, q, q0, ts, a) |> Some |> Option.filter validate
        
    let accepts (DFA (_, _, q0, ts, a)) (s: string) =
        let delta = ts |> List.map Transition.strip |> Map.ofList
        let deltaF state symbol = Map.find (state, symbol) delta
        let qr = s |> Seq.map Symbol |> Seq.toList |> List.fold deltaF q0
        (qr, List.contains qr a)