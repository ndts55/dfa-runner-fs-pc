﻿open FParsec
open Parser
open Models

[<EntryPoint>]
let main _ =
    
    let r = run dfa "init: q0
accept: q1, q2

q0, a, q1; q0,b,q2

q1, a, q1
q1, b, q0

q2, a, q0
q2, b, q2"

    match r with
    | Success (m, _, _) -> Option.iter (fun dfa1 -> DFA.accepts dfa1 "aaabbbaaa" |> printfn "%A") m
    | Failure(m, u, p) -> printfn "F:\n%A\n%A\n%A" m u p
    0
