module Parser

open FParsec
open Models

module private Commons =
    let ws = skipMany (skipChar ' ')
    let comma = ws .>> skipChar ',' .>> ws
    let colon = ws .>> skipChar ':' .>> ws
    let endStatement = (skipNewline <|> skipChar ';' <|> eof) >>. spaces

module private Partials =
    open Commons
    
    let isIdContinue c = isAsciiLetter c || isDigit c || c = '-'
    let state =
        identifier (
            IdentifierOptions(
                 isAsciiIdStart = isAsciiLetter,
                 isAsciiIdContinue = isIdContinue
            )
        )
        |>> State
        
    let symbol = asciiLetter <|> digit |>> Symbol
    let transition =
        state .>> comma .>>. symbol .>>. (comma >>. state)
        |>> Transition
    let transitionList = sepEndBy transition endStatement
    let init = skipString "init" >>. colon >>. state .>> endStatement


    let stateList = sepBy state comma

    let accept = skipString "accept" >>. colon >>. stateList .>> endStatement
    
open Partials

let dfa: Parser<DFA option, unit> = init .>>. accept .>>. transitionList |>> DFA.from